package org.springframework.hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MessagePrinter {

    @Autowired
    @Qualifier("service_en")
    private MessageService service;

    public void printMessage() {
        System.out.println(this.service.getMessage());
    }

    public MessageService getService() {
        return service;
    }

    public void setService(MessageService service) {
        this.service = service;
    }

}