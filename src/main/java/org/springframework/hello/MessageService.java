package org.springframework.hello;

public interface MessageService {
    String getMessage();
}